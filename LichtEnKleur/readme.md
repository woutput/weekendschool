Informatie over de les "Licht en kleur"

De les bestaat uit een aantal losse proefjes die onafhankelijk van elkaar kunnen worden uitgevoerd. Sommige proefjes kunnen de kinderen zelf uitvoeren. Met behulp van de proefjes leren we eigenschappen van licht en kleur.

1. Gekleurde lampjes
  * benodigdheden
    * donkere kamer
    * 