Informatie over de les "Hoe werkt een microchip?"

Voorbereiding
- Bekijk presentatie; sommige slides zijn screenshots van de filmpjes, optioneel te gebruiken als herhaling
- Test de presentatie ter plekke voor beeld en geluid; de presentatie bevat filmpjes
- Print de [stickers met de rollen](WKS%20CPU%20-%20rollen%20-%20print%20op%20A4%20stickervel%208x3%20is%2024%20stickers.docx)
- Print de [rollen en programmas](WKS%20CPU%20-%20rollen%20en%20programma-instructies%20-%20print%20op%20A4%20papier.docx)
