Informatie over de les "Wat zit er in een mobiele telefoon?"
============================================================

Lesdoelen
---------
* begrijpen dat een mobiele telefoon uit meerdere onderdelen bestaat die samenwerken
* oefenen om voorzichtig en veilig om te gaan met fijn gereedschap en kleine onderdelen
* presenteren voor een groep
* leren hoe ver de huidige techniek is: heel veel functionaliteit in een klein doosje

Voorbereiding / materiaal
-------------------------
* oude mobiele telefoons van ASML; bijvoorbeeld 1 per twee kinderen plus een paar reserve
* fijn gereedschap (kleine schroevendraaiers: plat, kruis en torx) van ASML
* Vellen A0-papier van weekendschool; bijvoorbeeld 1 per twee kinderen plus een paar reserve
* breed kleurloos transparant plakband (liefst in houder) van weekendschool; bijvoorbeeld 1 per twee kinderen plus een paar reserve
* veiligheidsbrillen van weekendschool
* eventueel: stiften van weekendschool

Stappenplan
-----------
1. Kort overzicht geven van de les
2. Uitleggen om voorzichtig en veilig te werk te gaan
3. Telefoons demonteren, onderdelen op A0-vel plakken, eventueel bij schrijven/tekenen met stift
4. Presentatie per groepje van twee (hulpvragen: Wat heb je geleerd? Wat doet dit onderdeel? Waarom zit dit onderdeel in een telefoon? Tips: wijs het onderdeel aan waar je over praat; vertel om de beurt; kijk het publiek aan)

Tips voor de docent
-------------------
* Plan tijd voor iedere stap van het stappenplan
* Vraag eventueel een begeleider om te helpen; vooral om de mobiele telefoons initieel open te maken
* Gebruik een lokaal dat voldoende groot is; tafels kunnen bijvoorbeeld handig in een U-vorm staan; midden voor kan de docent de uitleg van stappen 1 en 2 doen; later kunnen daar één voor één de groepjes hun presentatie houden met hun A3-vel
* Je kunt de kinderen enthousiasmeren door te vertellen over het aantal pixels in de camera-sensor, het aantal bits op een geheugenkaartje of de draadloze communicatie